# Kisphp Grid 

build results array for the example

```php
$results = array();
for ($i=1; $i<=3; $i++) {
    $results[] = array(
        'id'            => $i,
        'status'        => $i%3,
        'title'         => 'Title ' . $i,
        'hide_me'       => 'should be hidden',
        'start_date'    => '2015-01-20 00:00:00',
        'start_hour'    => '09:26:34',
        'end_date'      => '2015-03-30 10:00:00',
        'end_hour'      => '22:12:43',
    );
}
```

Begin with building the grid object and load array which will be displayed as table

```php
// build grid object
$grid = new Grid();
// add articles array
$grid->load($articles);
```

hide columns that you don't want to display in table

```php
$grid->columnHide(array(
    'id_category',
    'homepage',
    'comments',
));
// these hidden collumns will be available by appending "__" double underscore to them when used in append and prepend columns.
// every column that start with "__" underscore will not be displayed 
```

Add urls on columns, in this example, add the same url on two columns

```php

// $grid->addColumnUrl('array_with_columns', 'array_with_url_logic_schema');

$url = 'http://www.example.com/index.php?categ={id_category}';
$grid->addColumnUrl(
    array('categ', 'title'),
    array(
        'href' => $url,
        'class' => 'btn3 btn3-success btn-{status}',
        'id' => 'url_{id}',
    )
);
```

Change table rows header titles

```php

// $grid->columnsTitle('array_with_mappings_for_translation');

$grid->columnsTitle(array(
    'id'        => 'ID',
    'categ'     => 'Category',
    'title'     => 'Title',
    'content'   => 'Content'
));
```

Prepend a column for the table. You can use this to add edit, delete, etc options in admin

```php
// $grid->columnPrepend('row header', 'content');
// $grid->columnAppend('row header', 'content');

$grid->columnPrepend(
    'Options prepend',
    '<a class="delete" id="#delete-{id}" href="delete-{id}"><img src="/images/delete.gif" /></a>
    <a href="#edit-{id}"><img src="/images/edit.gif" /></a>
    <a href="#status-{__status}"><img src="/images/status-{__status}.gif" /></a>
    <a href="#home-{__homepage}"><img src="/images/home-{__homepage}.png" /></a>
    <a href="#comments-{__comments}"><img src="/images/comments-{__comments}.png" /></a>'
);
```

Change content of a column

```php

// $grid->columnTranslate('column_name', $array_with_mappings);

$grid->columnTranslate(
    'status',
    array(
        0 => '<span class="label label-danger">deleted</span>',
        1 => '<span class="label label-warning">offline</span>',
        2 => '<span class="label label-success">online</span>',
    )
);
```

add custom style on table columns
```php

$tableColumnOptions = array(
    array(
        'class' => 'table-options',
        'attr' => array(
            'width' => '100px',
        ),
    ),
    array(
        'class' => 'table-id',
        'attr' => array(
            'width' => '50px',
        ),
    ),
);

$grid = new Grid($tableColumnOptions);

```

add callback function

```php
function combineDateAndTime()
{
    $args = func_get_args();
    $date = DateTime::createFromFormat('Y-m-d G:i:s', $args[0][0]);
    $time = explode(':', $args[0][1]);
    $date->setTime($time[0], $time[1], $time[2]);

    return $date->format('Y-m-d G:i:s');
}
```

and use callback function to combine two or more columns (don't forget to hide some columns)

```php
$grid->columnsChange('start_date', array('start_date', 'start_hour'), 'combineDateAndTime');
```

Apply all changes to data array

```php
$grid->prepare();
```

Display html table

```php
echo $grid->drawTable();
```
