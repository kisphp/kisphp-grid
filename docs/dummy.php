<?php

$articles = [
    [
        'id' => 1,
        'status' => 2,
        'id_category' => 1,
        'is_homepage' => 1,
        'is_special' => 1,
        'title' => 'this is the title for article 1',
    ],
    [
        'id' => 2,
        'status' => 1,
        'id_category' => 2,
        'is_homepage' => 0,
        'is_special' => 1,
        'title' => 'this is the title for article 2',
    ],
];

use Kisphp\Grid;

$grid = new Grid();

// hide columns
$grid->columnHide(array(
    'status',
    'is_homepage',
    'is_special',
));


//

$grid->load($articles)->prepare();

$table = $grid->drawTable();
