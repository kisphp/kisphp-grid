.. Grid Table documentation master file, created by
   sphinx-quickstart on Tue Mar 10 11:05:03 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Grid Table's documentation!
======================================

Contents:

.. toctree::
   :maxdepth: 2


Grid bundle was designed as a helper for simpler display of table data.
It gets an array and displays it as a table.

For example we'll have this array:

.. literalinclude:: dummy.php
    :caption: Data array
    :language: php
    :lines: 1-21



Next let's create the grid table object:

.. literalinclude:: dummy.php
    :caption: Build object
    :language: php
    :lines: 1,21-25


Options on table rows and columns
=================================

Hide columns
************

.. literalinclude:: dummy.php
    :caption: Hide columns
    :language: php
    :lines: 1,25-32

.. warning::

    When hiding columns, you'll be able to access them by appending two underscores like `__status`, `__is_homepage`, `__is_special`.
    Columns starting with two underscores won't be displayed.


Indices and tables
==================

* :ref:`genindex`
* :ref:`search`

