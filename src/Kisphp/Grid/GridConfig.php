<?php

namespace Kisphp\Grid;

class GridConfig
{
    /**
     * @var array
     */
    protected $rows = [];

    /**
     * @var array
     */
    protected $tableAttributes = [];

    /**
     * addOption(0, 'class', 'row-class')
     * addOption(0, 'attr', ['width' => '120px])
     *
     * @param int $rowIndex
     * @param string $key
     * @param string $value
     *
     * @return $this
     */
    public function addOption($rowIndex, $key, $value)
    {
        $this->rows[$rowIndex][$key] = $value;

        return $this;
    }

    /**
     * @param int $rowIndex
     * @param array $options
     *
     * @return $this
     */
    public function setOptions($rowIndex, array $options)
    {
        $this->rows[$rowIndex] = $options;

        return $this;
    }

    /**
     * @return array
     */
    public function getRowsConfig()
    {
        return $this->rows;
    }

    /**
     * @param string $attribute
     * @param string $value
     *
     * @return $this
     */
    public function addTableAttributes($attribute, $value)
    {
        $this->tableAttributes[$attribute] = $value;

        return $this;
    }

    /**
     * @param array $tableAttributes
     */
    public function setTableAttributes($tableAttributes)
    {
        $this->tableAttributes = $tableAttributes;
    }

    /**
     * @return array
     */
    public function getTableAttributes()
    {
        return $this->tableAttributes;
    }
}
