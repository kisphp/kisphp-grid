<?php

namespace Kisphp\Grid;

class Grid
{
    /** @var string */
    protected $primaryKey = 'id';
    /** @var array */
    protected $columnUrls = [];
    /** @var array */
    protected $data = [];
    /** @var array */
    protected $columns_to_hide = [];
    /** @var array */
    protected $columns_to_translate = [];
    /** @var array */
    protected $columns_titles = [];
    /** @var array */
    protected $columns_to_combine = [];
    /** @var array */
    protected $columns_to_replace = [];
    /** @var array */
    protected $columns_to_append = [];
    /** @var array */
    protected $columns_to_prepend = [];
    /** @var array */
    protected $columns_options = [];
    /** @var array */
    protected $table_attributes = [];

    /**
     * $options = [
     *      [
     *          'class' => 'row-class',
     *          'attr' => ['width' => '120px']
     *      ],
     *      ['attr' => ['width' => '30px']],
     *      ['attr' => ['width' => '60px']],
     *  ]
     *
     * @param GridConfig $config
     */
    public function __construct(GridConfig $config = null)
    {
        if ($config !== null) {
            $this->columns_options = $config->getRowsConfig();
            $this->table_attributes = $config->getTableAttributes();
        }
    }

    /**
     * @return string
     */
    protected function getTableAttributes()
    {
        $attributes = '';
        foreach ($this->table_attributes as $k => $v) {
            if (!empty($v)) {
                $attributes .= sprintf(' %s="%s"', $k, $v);
            }
        }

        return $attributes;
    }

    /**
     * load results from database
     *
     * @param array $data
     *
     * @return $this
     */
    public function load(array $data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->primaryKey;
    }

    /**
     * @param string $primaryKey
     *
     * @return $this
     */
    public function setPrimaryKey($primaryKey)
    {
        $this->primaryKey = $primaryKey;

        return $this;
    }

    /**
     * add an url on a column in table
     *
     * @param array|string $column
     * @param array $url
     *
     * @return $this
     */
    public function addColumnUrl($column, array $url)
    {
        if (is_array($column) && count($column) > 0) {
            foreach ($column as $col) {
                $this->columnUrls[$col] = $url;
            }

            return $this;
        }

        $this->columnUrls[$column] = $url;

        return $this;
    }

    /**
     * change head title for columns
     *
     * @param array $dictionary
     *
     * @return $this
     */
    public function columnsTitle(array $dictionary)
    {
        $this->columns_titles = $dictionary;

        return $this;
    }

    /**
     * @param string $title
     * @param string $content
     *
     * @return string
     */
    public function columnPrepend($title, $content='')
    {
        $this->columns_to_prepend[$title] = $content;

        return $this->columns_to_prepend[$title];
    }

    /**
     * @param string $title
     * @param string $content
     *
     * @return string
     */
    public function columnAppend($title, $content='')
    {
        $this->columns_to_append[$title] = $content;

        return $this->columns_to_append[$title];
    }

    /**
     * @param string $text
     * @param array $dictionary
     *
     * @return string
     */
    protected function _translate($text, array $dictionary)
    {
        $text = urldecode($text);

        $tmp = [];
        if (count($dictionary) > 0) {
            foreach ($dictionary as $k => $v) {
                $tmp['{' . $k . '}'] = $v;
            }
        }
        $text = strtr($text, $tmp);
        unset($tmp);

        return $text;
    }

    /**
     * combine more columns into one by a callback function
     *
     * @param string $columnToDisplay
     * @param array $columnsToCombine
     * @param string|callable $callbackFunction
     *
     * @return $this
     */
    public function columnsCombine($columnToDisplay, array $columnsToCombine, $callbackFunction)
    {
        $this->columns_to_combine[$columnToDisplay] = [
            'columns' => $columnsToCombine,
            'callbackFunction' => $callbackFunction,
        ];

        return $this;
    }

    /**
     * apply all changes to array set
     */
    public function prepare()
    {
        $result = [];
        if (count($this->data) > 0) {
            $_columns_urls = array_keys($this->columnUrls);
            $i=0;
            // build rows
            foreach ($this->data as $row) {
                // build columns
                foreach ($row as $k => &$v) {
                    $text = $v;
                    $translated = false;

                    // combine collumns
                    if (isset($this->columns_to_combine[$k])) {
                        $params = [];
                        foreach ($this->columns_to_combine[$k]['columns'] as $p) {
                            $params[] = $row[$p];
                        }

                        $text = call_user_func($this->columns_to_combine[$k]['callbackFunction'], $params);
                        unset($params);
                    }

                    // replace columns
                    if (in_array($k, array_keys($this->columns_to_replace))) {
                        $text = $this->columns_to_replace[$k];
                    }

                    // make URL
                    if (in_array($k, $_columns_urls)) {
                        $url = '<a';
                        if (count($this->columnUrls[$k]) > 0) {
                            foreach ($this->columnUrls[$k] as $itemK => $itemV) {
                                $url .= ' ' . $itemK . '="' . $this->_translate($itemV, $row) . '"';
                            }
                        }
                        $url .= '>{__value__}</a>';
                        // add url to column row
                        if (!empty($this->columns_to_translate[$k])) {
                            $row['__' . $k] = $v;
                            $text = $this->_translate($url, [
                                '__value__' => strtr($v, $this->columns_to_translate[$k]),
                            ]);
                            $translated = true;
                        } else {
                            $text = $this->_translate($url, [
                                '__value__' => $v,
                            ]);
                        }
                    }

                    // apply translations for brackets variables from columns
                    if (in_array($k, array_keys($this->columns_to_translate)) && !$translated) {
                        $row['__' . $k] = $v;
                        $text = $this->columns_to_translate[$k][$text];
                    } else {
                        $text = $this->_translate($text, $row);
                    }

                    // keep only white listed columns
                    if (empty($this->columns_to_hide) || !in_array($k, $this->columns_to_hide)) {
                        // add result to data set
                        $result[$i][$k] = $text;
                    } else {
                        $result[$i]['__' . $k] = $text;
                    }
                }
                $i++;
            }
        }
        $this->data = $result;

        return $this;
    }

    /**
     * change content inside a column by an array logic
     *
     * @param string $columnName
     * @param array $dictionary
     *
     * @return $this
     */
    public function columnTranslate($columnName, array $dictionary=null)
    {
        if (is_array($dictionary) && !empty($dictionary) && !empty($columnName)) {
            $this->columns_to_translate[$columnName] = $dictionary;
        }

        return $this;
    }

    /**
     * @param string $columnName
     * @param string|int $newValue
     *
     * @return $this
     */
    public function columnReplace($columnName, $newValue)
    {
        if (!empty($columnName)) {
            $this->columns_to_replace[$columnName] = $newValue;
        }

        return $this;
    }

    /**
     * Hide these columns from display
     *
     * @param array $columns
     *
     * @return $this
     */
    public function columnHide(array $columns)
    {
        if (!empty($columns)) {
            $this->columns_to_hide = $columns;
        }

        return $this;
    }

    /**
     * return array data
     *
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param $columnName
     * @param $position
     *
     * @return string
     */
    protected function __tableHeadColumn($columnName, $position)
    {
        $result = '<th';
        if (isset($this->columns_options[$position]['attr']) && is_array($this->columns_options[$position]['attr'])) {
            $result .= ' style="';
            foreach ($this->columns_options[$position]['attr'] as $k => $v) {
                $result .= sprintf('%s: %s;', $k, $v);
            }
            $result .= '"';
        }
        if (isset($this->columns_options[$position]['class'])) {
            $result .= ' class="' . $this->columns_options[$position]['class'] . '"';
        }
        if (isset($this->columns_options[$position]['id'])) {
            $result .= ' id="' . $this->columns_options[$position]['id'] . '"';
        }
        $result .= '>';
        $result .= (!empty($this->columns_titles[$columnName])) ? $this->columns_titles[$columnName] : $columnName;
        $result .= '</th>';

        return $result;
    }

    /**
     * draws the table with all informations
     *
     * @return string
     */
    public function drawTable()
    {
        $result = '<table' . $this->getTableAttributes() . '>' . "\n";

        if (isset($this->data[0]) && is_array($this->data[0])) {
            $headers = array_keys($this->data[0]);

            $result .= '<tr>';
            $position = 0;

            // prepend columns
            if (count($this->columns_to_prepend) > 0) {
                $columnsTitles = array_keys($this->columns_to_prepend);
                foreach ($columnsTitles as $item) {
                    $result .= $this->__tableHeadColumn($item, $position);
                    $position++;
                }
            }

            foreach ($headers as $h) {
                if (strpos($h, '__') !== false) {
                    continue;
                }
                $result .= $this->__tableHeadColumn($h, $position);
                $position++;
            }

            // append columns
            if (count($this->columns_to_append) > 0) {
                $columnsTitles = array_keys($this->columns_to_append);
                foreach ($columnsTitles as $item) {
                    $result .= $this->__tableHeadColumn($item, $position);
                    $position++;
                }
            }

            $result .= '</tr>' . "\n";
        }

        foreach ($this->data as $row) {
            $result .= '<tr';
            if (isset($row[$this->getPrimaryKey()]) && !empty($row[$this->getPrimaryKey()])) {
                $result .= ' id="grid-row-' . $row[$this->getPrimaryKey()] . '"';
            }
            $result .= '>';

            // prepend columns
            if (count($this->columns_to_prepend) > 0) {
                foreach ($this->columns_to_prepend as $cp) {
                    $result .= '<td>' . $this->_translate($cp, $row) . '</td>';
                }
            }

            // columns from array
            foreach ($row as $k => $v) {
                if (strpos($k, '__') !== false) {
                    continue;
                }
                $result .= '<td>' . $v . '</td>';
            }

            // append columns
            if (count($this->columns_to_append) > 0) {
                foreach ($this->columns_to_append as $ca) {
                    $result .= '<td>' . $this->_translate($ca, $row) . '</td>';
                }
            }

            $result .= '</tr>' . "\n";
        }
        $result .= '</table>';

        return $result;
    }
}
