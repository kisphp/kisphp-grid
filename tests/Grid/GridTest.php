<?php

use Kisphp\Grid\Grid;

function combineDateAndTime()
{
    $format = 'Y-m-d H:i:s';
    $args = func_get_args();
    $date = DateTime::createFromFormat($format, $args[0][0]);
    $time = explode(':', $args[0][1]);
    $date->setTime($time[0], $time[1], $time[2]);

    return $date->format($format);
}

class GridTest extends PHPUnit_Framework_TestCase
{
    protected $results = array();

    public function setUp()
    {
        for ($i=1; $i<=1; $i++) {
            $this->results[] = array(
                'id'            => $i,
                'id_category'   => rand(1,10),
                'status'        => $i%3,
                'title'         => 'Title '.$i,
                'hide_me'       => 'should be hidden',
                'start_date'    => '2015-01-20 00:00:00',
                'start_hour'    => '09:26:34',
                'end_date'      => '2015-03-30 10:00:00',
                'end_hour'      => '22:12:43',
                'second_pk'     => 1000,
            );
        }
    }

    public function test_combine_columns()
    {
        $grid = new Grid();
        $grid->columnsCombine('start_date', array('start_date', 'start_hour'), 'combineDateAndTime');

        $grid->load($this->results);
        $grid->prepare();

        $res = $grid->getData();

        $this->assertSame('2015-01-20 09:26:34', $res[0]['start_date']);
    }

    public function test_hide_columns()
    {
        $grid = new Grid();
        $grid->columnHide(array(
            'start_hour',
            'end_hour',
        ));

        $grid->load($this->results);
        $grid->prepare();

        $res = $grid->getData();

        foreach ($res as $r) {
            $this->assertFalse(isset($r['start_hour']));
            $this->assertFalse(isset($r['end_hour']));
        }
    }

    public function test_column_url()
    {
        $config = new \Kisphp\Grid\GridConfig();
        $config->setTableAttributes([
            'class' => 'table',
        ]);
        $config->setOptions(1, [
            'attr' => ['background' => 'red'],
        ]);

        $grid = new Grid($config);

        $grid->addColumnUrl(
            'title',
            array(
                'href' => '/article/{id_category}',
                'class' => 'btn3 btn3-success btn-{status}',
                'id' => 'url_{id}',
            )
        );

        $grid->load($this->results);
        $grid->prepare();

        $res = $grid->getData();

        $this->assertNotContains('{id_category}',  $res[0]['title']);
    }

    public function test_multiple_columns_url()
    {
        $grid = new Grid();
        $grid->addColumnUrl(
            array('title','status'),
            array(
                'href' => '/article/{id}',
                'class' => 'btn3 btn3-success btn-{status}',
                'id' => 'url_{id}',
            )
        );

        $grid->load($this->results);
        $grid->prepare();

        $res = $grid->getData();

        $this->assertSame('<a href="/article/1" class="btn3 btn3-success btn-1" id="url_1">Title 1</a>', $res[0]['title']);
        $this->assertSame('<a href="/article/1" class="btn3 btn3-success btn-1" id="url_1">1</a>', $res[0]['status']);
    }

    public function test_translate_column()
    {
        $grid = new Grid();

        $statuses = array(
            0 => '<span class="label label-danger">deleted</span>',
            1 => '<span class="label label-warning">offline</span>',
            2 => '<span class="label label-success">online</span>',
        );

        $grid->columnTranslate(
            'status',
            $statuses
        );

        $grid->load($this->results);
        $grid->prepare();

        $res = $grid->getData();

        $i=1;
        foreach ($res as $r) {
            $this->assertSame($statuses[$i%3], $r['status']);
            $i++;
        }
    }

    public function test_translate_and_url_column()
    {
        $grid = new Grid();

        $statuses = array(
            0 => '<span class="label label-danger">deleted</span>',
            1 => '<span class="label label-warning">offline</span>',
            2 => '<span class="label label-success">online</span>',
        );
        $url = array(
            'href' => '#/article/{id}',
            'class' => 'btn3 btn3-success btn-{status}',
            'id' => 'url_{id}',
        );

        $grid->columnTranslate(
            'status',
            $statuses
        );

        $grid->addColumnUrl(
            'status',
            $url
        );

        $grid->load($this->results);
        $grid->prepare();

        $res = $grid->getData();

        foreach ($res as $r) {
            $this->assertContains('<a href="', $r['status']);
            $this->assertContains('<span class="label', $r['status']);
        }
    }

    public function test_prepend_column()
    {
        $grid = new Grid();

        $grid->columnPrepend('Prepend', 'prepended column');

        $grid->load($this->results)->prepare();

        $res = $grid->drawTable();

        $this->assertContains('<tr><th>Prepend</th>', $res, 'Should contain Prepend header');
        $this->assertContains('<td>prepended column</td>', $res, 'Should contain prepend row column');
    }

    public function test_prepend_column_url()
    {
        $grid = new Grid();

        $grid->columnHide(array('status'));
        $grid->columnPrepend('Prepend', '<a href="/articles/{__status}">my url</a>');

        $grid->load($this->results)->prepare();
        $res = $grid->drawTable();

        $this->assertContains('/articles/1', $res, 'Should contain changed URL');
    }

    public function test_prepend_second_column()
    {
        $grid = new Grid();

        $grid->columnPrepend('Prepend 1', 'prepended column 1');
        $grid->columnPrepend('Prepend 2', 'prepended column 2');

        $grid->load($this->results)->prepare();

        $res = $grid->drawTable();

        $this->assertContains('<tr><th>Prepend 1</th><th>Prepend 2</th>', $res, 'Should contain Prepend headers');
        $this->assertContains('<td>prepended column 1</td><td>prepended column 2</td>', $res, 'Should contain prepend row columns');
    }

    public function test_row_default_id()
    {
        $grid = new Grid();

        $grid->load($this->results)->prepare();

        $res = $grid->drawTable();

        $this->assertContains('<tr id="grid-row-1">', $res, 'Should contain grid-row-1 id on <tr>');
    }

    public function test_row_changed_id()
    {
        $grid = new Grid();

        $grid->setPrimaryKey('second_pk');

        $grid->load($this->results)->prepare();

        $res = $grid->drawTable();

        $this->assertContains('<tr id="grid-row-1000">', $res, 'Should contain grid-row-1000 id on <tr>');
    }

    public function test_replace_column()
    {
        $config = new \Kisphp\Grid\GridConfig();
        $config->setOptions(0, [
            'class' => 'asd',
            'id' => 'td-row',
            'attr' => [
                'background' => 'lightblue',
            ],
        ]);
        $grid = new Grid($config);
        $grid->columnReplace('title', 'this is my {id} title');

        $grid->columnAppend('options', 'my column');

        $grid->load($this->results)->prepare();

        $res = $grid->drawTable();

        $this->assertContains('this is my 1 title', $res);
        $this->assertContains('my column', $res);
        $this->assertContains('background: lightblue', $res);
        $this->assertContains('class="asd"', $res);
        $this->assertContains('id="td-row"', $res);
    }

    public function test_add_option()
    {
        $config = new \Kisphp\Grid\GridConfig();
        $config->setOptions(0, [
            'class' => 'asd',
            'id' => 'td-row',
        ]);

        $config->addOption(0, 'attr', ['background' => 'lightblue']);

        $grid = new Grid($config);
        $grid->columnReplace('title', 'this is my {id} title');

        $grid->columnAppend('options', 'my column');

        $grid->load($this->results)->prepare();

        $res = $grid->drawTable();

        $this->assertContains('this is my 1 title', $res);
        $this->assertContains('my column', $res);
        $this->assertContains('background: lightblue', $res);
        $this->assertContains('class="asd"', $res);
        $this->assertContains('id="td-row"', $res);
    }

    public function test_add_table_attributes()
    {
        $config = new \Kisphp\Grid\GridConfig();
        $config->setOptions(0, [
            'class' => 'asd',
            'id' => 'td-row',
        ]);

        $config->addTableAttributes('id', 'new-table-id');

        $grid = new Grid($config);
        $grid->columnReplace('title', 'this is my {id} title');

        $grid->columnAppend('options', 'my column');

        $grid->load($this->results)->prepare();

        $res = $grid->drawTable();

        $this->assertContains('this is my 1 title', $res);
        $this->assertContains('my column', $res);
        $this->assertContains('id="new-table-id"', $res);
        $this->assertContains('class="asd"', $res);
        $this->assertContains('id="td-row"', $res);
    }

    public function test_grid_columns_title()
    {
        $config = new \Kisphp\Grid\GridConfig();
        $config->setOptions(0, [
            'class' => 'asd',
            'id' => 'td-row',
        ]);

        $grid = new Grid($config);

        $grid->columnsTitle([
            'id_category' => 'Category ID',
            'status' => 'Status',
            'title' => 'Title',
        ]);

        $grid->load($this->results)->prepare();

        $res = $grid->drawTable();

        $this->assertContains('<th>Category ID</th>', $res);
        $this->assertContains('<th>Status</th>', $res);
        $this->assertContains('<th>Title</th>', $res);
    }
}
